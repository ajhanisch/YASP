# **YASP.ps1**  
Yet Another STIG Parser  
  
STIG / POAM management automation.  

Author: Ashton J. Hanisch < <ajhanisch@gmail.com> >  
  
# **SYNOPSIS**  
Script to help automate and provide structure to STIG / POAM management.

# **DESCRIPTION**  
Script designed to focus on STIG / POAM management and less on individual STIG compliance automation. Designed to help with automated relevant POAM generation, automated statistic calculation and presentation, automated STIG release / version comparison, and automated new STIG release handling.

# **FEATURES**  
* Built-in POAM Generation. 
  
Generates starter POAM documents from .csv files given to parse. Can be given as many exported STIG .csv files as needed to parse. Documents will contain working digital signature blocks for all needing to sign. Documents generated will be unique to vulnerability. If multiple hosts with the same STIG .csv file sharing multiple vulnerabilities, you will get one POAM for each vulnerability and have all effected hosts listed in that single POAM.  

* Built-in Statistics Generation.  
  
Generates statistics from .csv files given to parse. Can be given as many exported STIG .csv files as needed to parse. Statistics will be a breakdown of each 'Status' and 'Severity' as well as a total of CAT I, II, III. Statistics folder will contain a 'stats' file and a 'total' file. To view the statistics of all parsed STIG .csv's check the 'stats' file. To view each individual STIG results check their perspective 'total' file. Will also create a .csv file containing CAT I, CAT II, CAT III, Compliance Score, and Compliance Status for each parsed STIG.  

* Built-in Version Comparison.  
  
Compares a 'current' and a 'new' release of a particular STIG and outputs the vulnerabilities that are only in current, only in new, and in both.

* Built-in New STIG Release Handling
  
Generates POAM's from newly released STIG .csv files given when you already have POAM's created and working on from same STIG but earlier release. YASP will generate POAM's for vulnerabilities that are different and unique in the new STIG release, thus not overwriting or duplicating work you have already done.

# **CONSIDERATIONS**  
Ensure to have YASP.ps1, template_document.docx, and desired STIG Viewer exported .csv files in the same folder in order for proper functionality.  
  
# **DOCUMENTATION**  
Check out the Wiki for specific guidance using YASP.  

# **USAGE**  
Running the tool:  
.\YASP.ps1 [options]  
  
Typical Usage Example:  
.\YASP.ps1 -d  
  
Options:  
-d ......... Document generation switch parameter  
-s ......... Statistics generation switch parameter  
-c ......... Compare versions switch parameter  
-h ......... Show detailed help menu  

# **WISH LIST / TO DO**  
- [ ] Rewrite entire script to python to update / streamline code
- [ ] Create HTML reports of output
- [ ] Create better statistics output (master spreadsheet containing all data, HTML, etc.)
- [ ] Implement time based email notifications to review fully completed POAM documentation
- [ ] Implement centralized logging
- [ ] Implement automatic error reporting from script users
- [ ] Implement error trending / data mining analysis
  
