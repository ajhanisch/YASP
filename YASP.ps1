<#
.Synopsis
   Script to help automate STIG / POAM management.
.DESCRIPTION
   YASP will parse all .csv files and output a populated and formatted POAM word document containing all parsed data from .csv's for each relevant vulnerability as well as detailed vulnerability statistics about each STIG.
.PARAMETER d
    Document generation. This parameter tells the script you want to generate all necessary POAM documents from the .csv files you have placed in the same directory as the script. This parameter has built in verion / release handling functionality. That is to say it will only create new POAM's for STIG Vuln_ID's not yet created. Thus not duplicating or overwriting your currently working POAM's.
.PARAMETER s
    Statistics generation. This parameter tells the script you want to generate statistics for current working POAM's.
.PARAMETER c
    Comparison. This parameter tells the script you want to compare two versions / releases of a STIG and view differences.
.PARAMETER h
    Help page. This parameter tells the script you want to learn more about it. It will display this page after running the command 'Get-Help .\YASP.ps1 -Full' for you.
.INPUTS
   Document Generation (-d), Statistics Generation (-s), Comparison (-c), and Help (-h) flags are used as input.
.OUTPUTS
   Output includes the following folders, __log containing detailed logging and errors details, __poams containing generated POAM documents from -d flag use, __statistics containing text files with calculated statistics of finding totals for each STIG .csv parsed from -s flag use, __compare containing text files with the calculated differences between current and new STIG's parsed from -c flag use.
.NOTES
   NAME: YASP.ps1 (Yet Another STIG Parser)

   AUTHOR: Ashton J. Hanisch

   VERSION: 1.9

   TROUBLESHOOTING: All script output for each session will be in .\output\__logs\<run_date>\<run_date>_YASP.log. Should you have any problems script use, email ashton.j.hanisch.mil@mail.mil with a description of your issue and the log file that is associated with your problem.

   SUPPORT: For any issues, comments, concerns, ideas, contributions, etc. to any part of this script or its functionality, reach out to me at ashton.j.hanisch.mil@mail.mil. I am open to any thoughts you may have to make this work better for you or things you think are broken or need to be different. I will ensure to give credit where credit is due for any contributions or improvement ideas that are shared with me in the "Credits and Acknowledgements" section in the README.txt file.

   UPDATES: To check out any updates or revisions made to this script check out the updated README.txt included with this script.
#>

##############
# Parameters #
##############
param(
    [switch]$d,
    [switch]$s,
    [switch]$c,
    [switch]$h
)

#############
# Functions #
#############
function Populate-FindingInfo()
{
    # Directories
    $current_dir = (Get-Item -Path ".\" -Verbose).FullName

    # Arrays
    $finding_info = @() # Array to store results from all parsed .csv files

    # Test if current directory contains any .csv files or not. continue if yes exit if no
    if(Get-ChildItem $($current_dir) *.csv)
    {
        Get-ChildItem $($current_dir) -Recurse -Include *.csv |
            ForEach-Object {
                $source_data = Import-Csv $_.FullName
                foreach ( $source in $source_data )
                {
                    $finding = New-Object -TypeName PSObject
                    $finding | Add-Member -MemberType NoteProperty -Name Technology_Area -Value $source.TechnologyArea
                    $finding | Add-Member -MemberType NoteProperty -Name Host_Name -Value $source.HostName
                    $finding | Add-Member -MemberType NoteProperty -Name IP_Address -Value $source.IPAddress
                    $finding | Add-Member -MemberType NoteProperty -Name MAC_Address -Value $source.MACAddress
                    $finding | Add-Member -MemberType NoteProperty -Name Vuln_ID -Value $source.'Vuln ID'
                    $finding | Add-Member -MemberType NoteProperty -Name Severity -Value $source.Severity
                    $finding | Add-Member -MemberType NoteProperty -Name Group_Tite -Value $source.'Group Title'
                    $finding | Add-Member -MemberType NoteProperty -Name Rule_ID -Value $source.'Rule ID'
                    $finding | Add-Member -MemberType NoteProperty -Name STIG_ID -Value $source.'STIG ID'
                    $finding | Add-Member -MemberType NoteProperty -Name Rule_Title -Value $source.'Rule Title'
                    $finding | Add-Member -MemberType NoteProperty -Name Discussion -Value $source.Discussion
                    $finding | Add-Member -MemberType NoteProperty -Name IA_Control -Value $source.'IA Control'
                    $finding | Add-Member -MemberType NoteProperty -Name Check_Content -Value $source.'Check Content'
                    $finding | Add-Member -MemberType NoteProperty -Name Fix_Text -Value $source.'Fix Text'
                    $finding | Add-Member -MemberType NoteProperty -Name False_Positives -Value $source.'False Positives'
                    $finding | Add-Member -MemberType NoteProperty -Name False_Negatives -Value $source.'False Negatives'
                    $finding | Add-Member -MemberType NoteProperty -Name Documentable -Value $source.Documentable
                    $finding | Add-Member -MemberType NoteProperty -Name Mitigations -Value $source.Mitigations
                    $finding | Add-Member -MemberType NoteProperty -Name Potential_Impact -Value $source.'Potential Impact'
                    $finding | Add-Member -MemberType NoteProperty -Name Third_Party_Tools -Value $source.'Third Party Tools'
                    $finding | Add-Member -MemberType NoteProperty -Name Mitigation_Control -Value $source.'Mitigation Control'
                    $finding | Add-Member -MemberType NoteProperty -Name Responsibility -Value $source.Responsibility
                    $finding | Add-Member -MemberType NoteProperty -Name Severity_Override_Guidance -Value $source.'Severity Override Guidance'
                    $finding | Add-Member -MemberType NoteProperty -Name Check_Content_Reference -Value $source.'Check Content Reference'
                    $finding | Add-Member -MemberType NoteProperty -Name Classification -Value $source.'Classification'
                    $finding | Add-Member -MemberType NoteProperty -Name STIG -Value $source.'STIG'
                    $finding | Add-Member -MemberType NoteProperty -Name Status -Value $source.Status
                    $finding | Add-Member -MemberType NoteProperty -Name Comments -Value $source.Comments
                    $finding | Add-Member -MemberType NoteProperty -Name Finding_Details -Value $source.'Finding Details'
                    $finding | Add-Member -MemberType NoteProperty -Name Severity_Override -Value $source.'Severity Override'
                    $finding | Add-Member -MemberType NoteProperty -Name Severity_Override_Justification -Value $source.'Severity Override Justification'
                    $finding | Add-Member -MemberType NoteProperty -Name CCI -Value $source.CCI

                    $finding_info += $finding
                } # End for each finding in each csv parse loop
        } # End foreach csv parse loop
    }
    else
    {
        Write-Host ""
        Write-Host "[!] No .csv files in $($current_dir).Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again." ([char]7) -ForegroundColor Red
        Write-Host ""
    
        throw "[!] No .csv files in $($current_dir). Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again."
    } # End all csv in directory parse loop

    return $finding_info
}

function Generate-Documents($run_date, $organization, $date, $poc, $section, $responsibility)
{    
    # Directories
    $current_dir = (Get-Item -Path ".\" -Verbose).FullName
    $output_dir = "$($current_dir)\output" # Output folder to hold all script output in whatever folder it is ran in
    $poam_dir = "$($output_dir)\__poams" # Output folder to hold all generated poam documents

    # Arrays
    $finding_info = @() # Array to store results from all parsed .csv files
    $stigs = @() # Array to store parsed STIGs
    $working_documents = @() # Array to store results of already created documents
    $need_to_create = @() # Array to store results of vuln_id's not yet having generated POAM's
    $searched_for = @()  # Array to hold vuln_id's already searched for. Avoiding not needed POAM generation with multiple hosts from the same STIG being parsed.
    $nf_na = @() # Array to hold finding's to easily calculate statistics
   
    if(!(Test-Path $poam_dir))
    {
        New-Item -ItemType Directory -Force -Path $($poam_dir) > $null
    }

    # Populate finding_info
    $finding_info = Populate-FindingInfo

    # Gather data for documents already created 
    foreach($c in $finding_info)
    {
        $vuln_id_poam_dir_check = (Get-ChildItem -Path $($poam_dir) -filter "*_$($c.Vuln_ID).docx" | Measure-Object).Count
       
        if( $($vuln_id_poam_dir_check) -eq 1 -and $($searched_for) -notcontains $($c.Vuln_ID))
        {
            if($($working_documents) -notcontains $($c.Vuln_ID))
            {
                Write-Host "[*] $($c.Vuln_ID) for $($c.STIG) is already created." -ForegroundColor Green
                $working_documents += $($c.Vuln_ID)

                if($($searched_for) -notcontains $($c.Vuln_ID))
                {
                    $searched_for += $($c.Vuln_ID)
                }
            }
        }
        elseif( $($vuln_id_poam_dir_check) -eq 0 -and $($searched_for) -notcontains $($c.Vuln_ID))
        {
            if($($c.Status) -ne 'Not A Finding' -and $($c.Status) -ne 'Not Applicable')
            {
                if($($need_to_create) -notcontains $($c.Vuln_ID))
                {
                    Write-Host "[#] $($c.Vuln_ID) for $($c.STIG) needs to be created." -ForegroundColor Yellow
                    $need_to_create += $c

                    if($($searched_for) -notcontains $($c.Vuln_ID))
                    {
                        $searched_for += $($c.Vuln_ID)
                    }
                }   
            }
            elseif($($c.Status) -eq 'Not A Finding' -or $($c.Status) -eq 'Not Applicable')
            {
                if($($nf_na) -notcontains $($c.Vuln_ID))
                {
                    $nf_na += $c
                }
            }
        }
    }         

    # Present data for documents already created
    if($($working_documents))
    {
        $total_poams = (Get-ChildItem -Path $($poam_dir) -Filter "*.docx" | Measure-Object).Count
        $total_to_create = @($($need_to_create)).Length
        $documents_created = 0

        if($($total_to_create) -eq 0)
        {
            Write-Host ""
            Write-Host "[^] Looks like we have $($total_poams) POAM(s) generated already with $($total_to_create) POAM(s) to create with current csv's in $($current_dir)." -ForegroundColor Cyan
            Write-Host "[*] No new POAM(s) to create. Exiting now ..." -ForegroundColor Green
            exit 0
        }
        elseif($($total_to_create) -gt 0)
        {
            Write-Host ""
            Write-Host "[^] Looks like we have $($total_poams) POAM(s) generated already with $($total_to_create) POAM(s) to create with current csv's in $($current_dir)." -ForegroundColor Cyan
            Write-Host ""
            Write-Host -NoNewline "[^] Continue? (Y/N): "
            $response = Read-Host
            Switch ($($response))
            {
                'Y'{ Write-Host "[*] You have chosen to continue. Generating $($total_to_create) POAM(s) now ..." -ForegroundColor Green; Continue }
                'N'{ Write-Host "[*] You have chosen to exit. Exiting now." -ForegroundColor Green; exit 0 }
                default { Write-Host "[!] Incorrect response. Try a correct response next time." -ForegroundColor Red; exit 0 }
            }
        }
    }
    else
    {
        $total_to_create = @($($need_to_create)).Length
        $documents_created = 0

        Write-Host ""
        Write-Host "[^] Looks like we have $($total_to_create) POAM(s) to create ..." -ForegroundColor Cyan
        Write-Host ""
        Write-Host -NoNewline "[^] Continue? (Y/N): " -ForegroundColor Cyan
        $response = Read-Host
        Switch ($($response))
        {
            'Y'{ Write-Host "[*] You have chosen to continue. Generating $($total_to_create) POAM(s) now ..." -ForegroundColor Green; Continue }
            'N'{ Write-Host "[*] You have chosen to exit. Exiting now." -ForegroundColor Green; exit 0 }
            default { Write-Host "[!] Incorrect response. Try a correct response next time." -ForegroundColor Red; exit 0 }
        }
    }

    # Get signatures from template
    $word = New-Object -ComObject Word.Application 
    $word.Visible = $false
    #$word.Visible = $true
    $doc = $word.Documents.Open("$($current_dir)\signature_template.docx")
    $sel = $word.Selection

    # Select entire document contents
    $doc.Select()

    # Copy contents to clipboard
    $sel.Copy()

    # Close document
    $word.Quit()

    <# Disabling this part to test fix for common issue.
    # Quit cleanly
    $null = [System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$word)
    [gc]::Collect()
    [gc]::WaitForPendingFinalizers()
    Remove-Variable word
    #>

    # Elapsed time stop watch initilization
    $stop_watch = [system.diagnostics.stopwatch]::startNew()
        
    # Clear the screen to get ready to display progress information to user
    cls

    # Generate or skip document
    foreach($c in $need_to_create)
    {
        # Populate array of STIGs
        if($c.STIG -notin $stigs)
        {
            $stigs += $c.STIG
        }

        # Remove bad characters from the STIG so we can save the POAM file without file name error
        $corrected_stig = $c.STIG -replace "[:\//]",""

        if($c.Severity -eq 'high')
        {
            $category = "CAT 1"
        }
    
        elseif($c.Severity -eq 'medium')
        {
            $category = "CAT 2"
        }

        elseif($c.Severity -eq 'low')
        {
            $category = "CAT 3"
        }

        $vuln_id_check = (Get-ChildItem -Path $($poam_dir) | Where-Object { $_.Name -match $($c.Vuln_ID) }).Length
        
        # Test if vuln_id exists in output_dir
        if( $($vuln_id_check) -gt 0)
        {
            Continue
        }
        elseif($($vuln_id_check) -eq 0)
        {
            $effected_hosts = ($finding_info | Where-Object { $_.Vuln_ID -match $($c.Vuln_ID) -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'})

            # Create POAM word document object
            $Word = New-Object -ComObject word.application

            # Make object visible or not
            $Word.visible = $false
            #$Word.visible = $True

            # Add document to object
            $Document = $Word.documents.add()

            # Set margins for document to narrow
            $Word.ActiveDocument.PageSetup.TopMargin = 36
            $Word.ActiveDocument.PageSetup.BottomMargin = 36
            $Word.ActiveDocument.PageSetup.LeftMargin = 36
            $Word.ActiveDocument.PageSetup.RightMargin = 36

            # Start building document
            $Selection = $Word.Selection

            $Selection.Font.Bold = 1
            $Selection.Paragraphs.Alignment = 1
            $Selection.TypeText("PLAN OF ACTION & MILESTONES REQUEST FORM (POAM)")
            $Selection.Font.Bold = 0
            $Selection.TypeParagraph()

            $Selection.Font.Color = 32768
            $Selection.TypeText("UNCLASSIFIED/FOUO")
            $Selection.Font.Color = 0
            $Selection.TypeParagraph()
            $Selection.Paragraphs.Alignment = 0

            $Selection.Font.Underline = 1
            $Selection.TypeText("Organization:")
            $Selection.Font.Underline = 0
            $Selection.TypeText("`t"*10)

            $Selection.Font.Underline = 1
            $Selection.TypeText("Date:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeText("$($organization)")
            $Selection.TypeText("`t"*10)
            $Selection.TypeText("$($date)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("POC:")
            $Selection.Font.Underline = 0
            $Selection.TypeText("`t"*11)
            $Selection.Font.Underline = 1
            $Selection.TypeText("Section:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeText("$($poc)")
            $Selection.TypeText("`t"*9)
            $Selection.TypeText("$($section)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("STIG / Checklist Name:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeText("$($c.STIG)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Rule Title:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeText("$($c.Rule_Title)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("STIG ID:")
            $Selection.Font.Underline = 0
            $Selection.TypeText("`t"*6)

            $Selection.Font.Underline = 1
            $Selection.TypeText("Rule ID:")
            $Selection.Font.Underline = 0
            $Selection.TypeText("`t"*5)

            $Selection.Font.Underline = 1
            $Selection.TypeText("Vuln ID:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()

            # Test to properly align the text. Aligns different when ID's are longer or shorter.
            if($($c.STIG_ID).length -gt 13)
            {
                $Selection.TypeText("$($c.STIG_ID)")
                $Selection.TypeText("`t"*4)   
            }
        
            elseif($($c.STIG_ID.length -lt 13))
            {
                $Selection.TypeText("$($c.STIG_ID)")
                $Selection.TypeText("`t"*5)
            }

            if($($c.Rule_ID).length -gt 24)
            {
                $Selection.TypeText("$($c.Rule_ID)")
                $Selection.TypeText("`t"*2)
            }
            elseif($($c.Rule_ID).length -lt 24)
            {
                $Selection.TypeText("$($c.Rule_ID)")
                $Selection.TypeText("`t"*3)
            }

            $Selection.TypeText("$($c.Vuln_ID)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Severity:")
            $Selection.Font.Underline = 0
            $Selection.TypeText("`t"*10)
            $Selection.Font.Underline = 1
            $Selection.TypeText("Responsibility:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()

            $Selection.TypeText("$($category)")
            $Selection.TypeText("`t"*11)
            $Selection.TypeText("$($responsibility)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Effected Host(s):")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeText($($effected_hosts.Host_Name) -join ", ")

            $Selection.TypeParagraph()
            $Selection.Font.Underline = 1
            $Selection.TypeText("Rule Discussion:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            # Replace new lines in this variable with spaces to avoid huge space taken in document.
            #$formatted_rule_discussion = $($c.Discussion).Replace("`n"," ")
            $formatted_rule_discussion = $($c.Discussion) -replace "`n"," "
            $Selection.TypeText("$($formatted_rule_discussion)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Check Content:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            # Replace new lines in this variable with spaces to avoid huge space taken in document.
            $formatted_check_content = $($c.Check_Content) -replace "`n"," "
            $Selection.TypeText("$($formatted_check_content)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Fix Text:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            # Replace new lines in this variable with spaces to avoid huge space taken in document.
            $formatted_fix_text = $($c.Fix_Text) -replace "`n"," "
            $Selection.TypeText("$($formatted_fix_text)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Finding Details:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            # Replace new lines in this variable with spaces to avoid huge space taken in document.
            $formatted_finding_details = $($c.Finding_Details) -replace "`n"," "
            $Selection.TypeText("$($formatted_finding_details)")
            $Selection.TypeParagraph()
        
            $Selection.Font.Underline = 1
            $Selection.TypeText("Comments:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            # Replace new lines in this variable with spaces to avoid huge space taken in document.
            $formatted_comments = $($c.Comments) -replace "`n"," "
            $Selection.TypeText("$($formatted_comments)")
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Justification (Why control cannot be implemented):")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Mitigation Plan:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Milestones:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeParagraph()

            $Selection.Font.Underline = 1
            $Selection.TypeText("Completion Date:")
            $Selection.Font.Underline = 0
            $Selection.TypeParagraph()
            $Selection.TypeParagraph()
            $Selection.TypeParagraph()

            # Paste template contents to POAM document
            $Selection.Paste()

            # Save document
            $file_name = "$($poam_dir)\$($corrected_stig)_$($category)_$($c.Vuln_ID).docx"
            $Document.SaveAs([ref]$file_name,[ref]$SaveFormat::wdFormatDocument)
        
            # Cleanly quit
            $Word.Quit()
            $null = [System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$Word)
            [gc]::Collect()
            [gc]::WaitForPendingFinalizers()
            Remove-Variable Word 

            $documents_created += 1

            # Present stats and continue
            $percent_complete = ($($documents_created)/$($total_to_create)).ToString("P")
            $estimated_time = (($($total_to_create) - $($documents_created)) * 5.7 / 60)
            $formatted_estimated_time = [math]::Round($estimated_time,2)
            $elapsed_time = $stop_watch.Elapsed.ToString('dd\.hh\:mm\:ss')
                
            for($i = $documents_created; $i -le $total_to_create; $i++)
            {
                Write-Progress -Activity "Generating POAM(s) ..." -Status "$($category) $($c.Vuln_ID) for $($c.Host_Name) ..." -PercentComplete ($($documents_created)/$($total_to_create)*100) -CurrentOperation "$($percent_complete) complete          ~$($formatted_estimated_time) minute(s) left          $($documents_created)/$($total_to_create) created          $($elapsed_time) time elapsed"
            }
        } # End elseif
        else
        {
            Write-Host ""
            Write-Host "[!] No .csv files in $($current_dir).Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again." ([char]7) -ForegroundColor Red
            Write-Host ""    
            throw "[!] No .csv files in $($current_dir). Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again."
        }
    } # End foreach c in need_to_create
} # End Generate-Documents function

function Generate-Statistics($run_date)
{
    # Directories
    $current_dir = (Get-Item -Path ".\" -Verbose).FullName
    $output_dir = "$($current_dir)\output" # Output folder to hold all script output in whatever folder it is ran in
    $poam_dir = "$($output_dir)\__poams" # Output folder to hold all generated poam documents
    $stats_dir = "$($output_dir)\__statistics\$($run_date)" # Statistics folder to hold all parsed statistics

# CSS
$css = 
@"
<style>
h1, h5, th { text-align: center; font-family: Segoe UI; }
table { margin: auto; font-family: Segoe UI; box-shadow: 10px 10px 5px #888; border: thin ridge grey; }
th { font-size: 20px; background: #0046c3; color: #fff; max-width: 400px; padding: 5px 10px; }
td { font-size: 15px; padding: 5px 20px; color: #000; }
tr { background: #b8d1f3; }
tr:nth-child(even) { background: #dae5f4; }
tr:nth-child(odd) { background: #b8d1f3; }
</style>
"@
  
    # Files
    $stats_file = "$($stats_dir)\$($run_date)_detailed.txt" # Stats file to hold each STIG's individual stats
    $ia_csvs_stats = "$($stats_dir)\$($run_date)_overview.csv"
    $html_report = "$($stats_dir)\$($run_date)_report.html"
    $detailed_stats_file = "$($stats_dir)\$($run_date)_detailed.csv" # Stats csv to hold each STIG's detailed invividual stats

    # Arrays
    $stigs = @() # Array to store parsed STIGs
    $working_documents = @() # Array to store results of already created documents
    $need_to_create = @() # Array to store results of vuln_id's not yet having generated POAM's
    $searched_for = @()  # Array to hold vuln_id's already searched for. Avoiding not needed POAM generation with multiple hosts from the same STIG being parsed.
    $nf_na = @() # Array to hold finding information that has status of "not a finding" and "not applicable" to make statistics calculations easier

    # Populate finding_info
    $finding_info = Populate-FindingInfo

    # Test if current directory contains .csv files or not. continue if yes exit if no
    if(Get-ChildItem $($current_dir) *.csv)
    {
        if(!(Test-Path $stats_dir))
        {
            New-Item -ItemType Directory -Force -Path $($stats_dir) > $null
        }

        # Gather data for documents already created 
        foreach($c in $finding_info)
        {
            if($($c.Status) -ne 'Not A Finding' -and $($c.Status) -ne 'Not Applicable' )
            {
                if($($need_to_create) -notcontains $($c.Vuln_ID) -and $($searched_for) -notcontains $($c.Vuln_ID))
                {
                    $need_to_create += $c
                    $searched_for += $($c.Vuln_ID)
                }
            }
            elseif($($c.Status) -eq 'Not A Finding' -or $($c.Status) -eq 'Not Applicable')
            {
                if($($nf_na) -notcontains $($c.Vuln_ID) -and $($searched_for) -notcontains $($c.Vuln_ID))
                {
                    $nf_na += $c
                    $searched_for += $($c.Vuln_ID)
                }
            }
        }

        # Populate array of STIGs
        foreach($c in $finding_info)
        {
            if($c.Status -ne 'Not A Finding' -and $c.Status -ne 'Not Applicable')
            {
                if($c.STIG -notin $stigs)
                {
                    $stigs += $c.STIG
                }
            }
        }

        $stig_open_high = 0
        $stig_open_medium = 0
        $stig_open_low = 0
        $total_open_high = @($need_to_create | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'high' }).Length
        $total_open_medium = @($need_to_create | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'medium' }).Length
        $total_open_low = @($need_to_create | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'low' }).Length
        $open = @($($total_open_high) + $($total_open_medium) + $($total_open_low))

        $stig_nr_high = 0
        $stig_nr_medium = 0
        $stig_nr_low = 0
        $total_nr_high = @($need_to_create | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'high' }).Length
        $total_nr_medium = @($need_to_create | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'medium' }).Length
        $total_nr_low = @($need_to_create | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'low' }).Length
        $nr = @($($total_nr_high) + $($total_nr_medium) + $($total_nr_low))

        $stig_nf_high = 0
        $stig_nf_medium = 0
        $stig_nf_low = 0
        $total_nf_high = @($nf_na | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'high' }).Length
        $total_nf_medium = @($nf_na | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'medium' }).Length
        $total_nf_low = @($nf_na | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'low' }).Length
        $nf = @($($total_nf_high) + $($total_nf_medium) + $($total_nf_low))

        $stig_na_high = 0
        $stig_na_medium = 0
        $stig_na_low = 0
        $total_na_high = @($nf_na | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'high' }).Length
        $total_na_medium = @($nf_na | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'medium' }).Length
        $total_na_low = @($nf_na | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'low' }).Length
        $na = @($($total_na_high) + $($total_na_medium) + $($total_na_low))

        $total = ( $($open) + $($nr) + $($nf) )

        $cat_I_total = @($need_to_create | Where { $_.Severity -eq 'high' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable' }).Length
        $cat_II_total = @($need_to_create | Where { $_.Severity -eq 'medium' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'}).Length
        $cat_III_total = @($need_to_create | Where { $_.Severity -eq 'low' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'}).Length

        # Check to avoid dividing by 0. If NF = 0 then there are no findings fixed, meaning 0 % compliance.
        if($($nf) -eq 0)
        {
            $compliance_score = "0 %"
        }
        elseif($($nf) -gt 0)
        {
            $compliance_score = ($($nf) / $($total)).ToString("P")
        }

        # Calculate and present "Compliance Status"
        if($($compliance_score) -eq "100 %")
        {
            $compliance_status = "BLUE"
        }
        elseif($($compliance_score) -ge "90 %")
        {
            $compliance_status = "GREEN"
        }
        elseif($($compliance_score) -ge "80 %" )
        {
            $compliance_status = "YELLOW"
        }
        elseif($($compliance_score) -ge "0 %" )
        {
            $compliance_status = "RED"
        }

        $stig_count = @($($stigs)).Length
        
        # Generate .TXT's of results
        # Total of all STIGs
$x = 
@"
=========================================================
Total from $($stig_count) STIG(s)
$(
$stig_number = 0
foreach($s in $stigs)
{
	$stig_number += 1
	$("($stig_number) $s" | Out-String)
}
)
Open / High: $($total_open_high)
Open / Medium: $($total_open_medium)
Open / Low: $($total_open_low)
----------------------
Total Open: $($open)

Not Reviewed / High: $($total_nr_high)
Not Reviewed / Medium: $($total_nr_medium)
Not Reviewed / Low: $($total_nr_low)
----------------------
Total Not Reviewed: $($nr)

Not A Finding / High: $($total_nf_high)
Not A Finding / Medium: $($total_nf_medium)
Not A Finding / Low: $($total_nf_low)
----------------------
Total Not A Finding: $($nf)

Not Applicable / High: $($total_na_high)
Not Applicable / Medium: $($total_na_medium)
Not Applicable / Low: $($total_na_low)
----------------------
Total Not Applicable: $($na)

CAT I Total: $($cat_I_total)
CAT II Total: $($cat_II_total)
CAT III Total: $($cat_III_total)

Compliance Score Overall: $($compliance_score)
Compliance Status Overall: $($compliance_status)
=========================================================
"@

        $x | Out-File -FilePath $($stats_file) -Append

        # Add headers to ia_csvs_stats
        Add-Content -Path $($ia_csvs_stats) -Value 'STIG, CAT_I_COUNT, CAT_II_COUNT, CAT_III_COUNT, COMPLIANCE_SCORE, COMPLIANCE_STATUS'

        #Add headers to detailed_stats_file
        Add-Content -Path $($detailed_stats_file) -Value 'STIG, OPEN_HIGH, OPEN_MEDIUM, OPEN_LOW, NR_HIGH, NR_MEDIUM, NR_LOW, NF_HIGH, NF_MEDIUM, NF_LOW, NA_HIGH, NA_MEDIUM, NA_LOW, TOTAL_OPEN, TOTAL_NR, TOTAL_NF, TOTAL_NA, CAT_I_TOTAL, CAT_II_TOTAL, CAT_III_TOTAL, COMPLIANCE_SCORE, COMPLIANCE_STATUS'

        $stig_number = 0
        foreach($s in $stigs)
        {
            $stig_number += 1
            $corrected_stig = $s -replace "[:\//]",""
            $total_file = "$($stats_dir)\$($corrected_stig).txt"

            $open_high = @($need_to_create -match $s | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'high' }).Length
            $open_medium = @($need_to_create -match $s | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'medium' }).Length
            $open_low = @($need_to_create -match $s | Where { $_.Status -eq 'Open' -and $_.Severity -eq 'low' }).Length

            $nr_high = @($need_to_create -match $s | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'high' }).Length
            $nr_medium = @($need_to_create -match $s | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'medium' }).Length
            $nr_low = @($need_to_create -match $s | Where { $_.Status -eq 'Not Reviewed' -and $_.Severity -eq 'low' }).Length

            $nf_high = @($nf_na -match $s | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'high'}).Length
            $nf_medium = @($nf_na -match $s | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'medium'}).Length
            $nf_low = @($nf_na -match $s | Where { $_.Status -eq 'Not A Finding' -and $_.Severity -eq 'low'}).Length

            $na_high = @($nf_na -match $s | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'high'}).Length
            $na_medium = @($nf_na -match $s | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'medium'}).Length
            $na_low = @($nf_na -match $s | Where { $_.Status -eq 'Not Applicable' -and $_.Severity -eq 'low'}).Length

            $stig_open_high += $($open_high)
            $stig_open_medium += $($open_medium)
            $stig_open_low += $($open_low)
            $total_open = ($($open_high) + $($open_medium) + $($open_low))

            $stig_nr_high += $($nr_high)
            $stig_nr_medium += $($nr_medium)
            $stig_nr_low += $($nr_low)
            $total_nr = ($($nr_high) + $($nr_medium) + $($nr_low))

            $stig_nf_high += $($nf_high)
            $stig_nf_medium += $($nf_medium)
            $stig_nf_low += $($nf_low) 
            $total_nf = ($($nf_high) + $($nf_medium) + $($nf_low))

            $stig_na_high += $($na_high)
            $stig_na_medium += $($na_medium)
            $stig_na_low += $($na_low) 
            $total_na = ($($na_high) + $($na_medium) + $($na_low))

            $total_total = ( $($total_open) + $($total_nr) + $($total_nf) )

            $cat_I_count = @($need_to_create -match $s | Where { $_.Severity -eq 'high' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'}).Length
            $cat_II_count = @($need_to_create -match $s | Where { $_.Severity -eq 'medium' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'}).Length
            $cat_III_count = @($need_to_create -match $s | Where { $_.Severity -eq 'low' -and $_.Status -ne 'Not A Finding' -and $_.Status -ne 'Not Applicable'}).Length

            # Check to avoid dividing by 0. If NF = 0 then there are no findings fixed, meaning 0% compliance.
            if($($total_nf) -eq 0)
            {
                $compliance_score = "0 %"
            }
            elseif($($total_nf) -gt 0)
            {
                $compliance_score = ($($total_nf) / $($total_total)).ToString("P")
            }

            # Calculate and present "Compliance Status"
            if($($compliance_score) -eq "100 %")
            {
                $compliance_status = "BLUE"
            }
            elseif($($compliance_score) -ge "90 %")
            {
                $compliance_status = "GREEN"
            }
            elseif($($compliance_score) -ge "80 %" )
            {
                $compliance_status = "YELLOW"
            }
            elseif($($compliance_score) -ge "0 %" )
            {
                $compliance_status = "RED"
            }

            # Append information to ia_csvs_stats
            "{0}, {1}, {2}, {3}, {4}, {5}" -f $($s), $($cat_I_count), $($cat_II_count), $($cat_III_count), $($compliance_score), $($compliance_status) | Add-Content -Path $($ia_csvs_stats)
             
            # Append information to detailed_stats_file
            "{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}" -f $($s), $($open_high), $($open_medium), $($open_low), $($total_open), $($nr_high), $($nr_medium), $($nr_low), $($total_nr), $($nf_high), $($nf_medium), $($nf_low), $($total_nf), $($na_high), $($na_medium), $($na_low), $($total_na), $($cat_I_count), $($cat_II_count), $($cat_III_count), $($compliance_score), $($compliance_status) | Add-Content -Path $($detailed_stats_file)


            # Output each STIG stats file to stats file
$x = 
@"
=========================================================
STIG ($stig_number): $($s) 
 
Open / High: $($open_high) 
Open / Medium: $($open_medium) 
Open / Low: $($open_low) 
----------------------
Total Open: $($total_open) 
 
Not Reviewed / High: $($nr_high) 
Not Reviewed / Medium: $($nr_medium) 
Not Reviewed / Low: $($nr_low) 
----------------------
Total Not Reviewed: $($total_nr) 
 
Not A Finding / High: $($nf_high) 
Not A Finding / Medium: $($nf_medium) 
Not A Finding / Low: $($nf_low) 
----------------------
Total Not A Finding: $($total_nf) 
 
Not Applicable / High: $($na_high)
Not Applicable / Medium: $($na_medium)
Not Applicable / Low: $($na_low)
----------------------
Total Not Applicable: $($total_na)
 
CAT I Total: $($cat_I_count)
CAT II Total: $($cat_II_count)
CAT III Total: $($cat_III_count)
 
Compliance Score: $($compliance_score)
Compliance Status Overall: $($compliance_status)
=========================================================
"@
            $x | Out-File -FilePath $($stats_file) -Append
    
            # Output each STIG totals to totals file
$x = 
@"
=========================================================
STIG ($stig_number): $($s)

Open / High: $($open_high)
Open / Medium: $($open_medium)
Open / Low: $($open_low)
----------------------
Total Open: $($total_open)

Not Reviewed / High: $($nr_high)
Not Reviewed / Medium: $($nr_medium)
Not Reviewed / Low: $($nr_low)
----------------------
Total Not Reviewed: $($total_nr)

Not A Finding / High: $($nf_high)
Not A Finding / Medium: $($nf_medium)
Not A Finding / Low: $($nf_low)
----------------------
Total Not A Finding: $($total_nf)

Not Applicable / High: $($na_high)
Not Applicable / Medium: $($na_medium)
Not Applicable / Low: $($na_low)
----------------------
Total Not Applicable: $($total_na)

CAT I Total: $($cat_I_count)
CAT II Total: $($cat_II_count)
CAT III Total: $($cat_III_count)

Compliance Score: $($compliance_score)
Compliance Status Overall: $($compliance_status)
=========================================================
"@

        $x | Out-File -FilePath $($total_file) -Append

        } # End foreach stig in stigs

        # Generate HTML Report
        Import-Csv $($ia_csvs_stats) | ConvertTo-Html -Head $($css) -Body "<h1>YASP Statistics Report</h1> <h5>Generated on $($run_date)</h5>" | Out-File $($html_report)
    } 
    else
    {
        Write-Host ""
        Write-Host "[!] No .csv files in $($current_dir). Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again." ([char]7) -ForegroundColor Red
        Write-Host ""    
        throw "[!] No .csv files in $($current_dir). Please ensure to place desired STIG Viewer exported .csv files in $($current_dir) and try again."
    }
} # End Generate-Statistics function


function Compare-Versions($run_date, $current, $new)
{
    # Directories
    $current_dir = (Get-Item -Path ".\" -Verbose).FullName
    $output_dir = "$($current_dir)\output" # Output folder to hold all script output in whatever folder it is ran in
    $compare_dir = "$($output_dir)\__compare\$($run_date)" # Comparison folder to hold all compared file output

    # Files
    $compare_file = "$($compare_dir)\$($run_date)_compare_file.txt" # Stats file to hold compared file output

    if(!(Test-Path $($compare_dir)))
    {
        New-Item -ItemType Directory -Force -Path $($compare_dir) > $null
    }

    $current_version = Import-Csv $current
    $new_release = Import-Csv $new

    $current_version_stig_name = $($current_version.STIG) | Select-Object -First 1
    $new_release_stig_name = $($new_release.STIG) | Select-Object -First 1

    $only_in_current = Compare-Object $current_version $new_release -Property 'Vuln ID' -PassThru | ? { $_.SideIndicator -eq "<=" } | Sort-Object 'Vuln ID' | Select-Object *
    $only_in_new = Compare-Object $current_version $new_release -Property 'Vuln ID' -PassThru | ? { $_.SideIndicator -eq "=>" } | Sort-Object 'Vuln ID' | Select-Object *
    $in_both = Compare-Object $current_version $new_release -Property 'Vuln ID' -PassThru -IncludeEqual | ? { $_.SideIndicator -eq "==" } | Sort-Object 'Vuln ID' | Select-Object *

    $only_in_current_count = @($($only_in_current)).Length
    $only_in_new_count = @($($only_in_new)).Length
    $in_both_count = @($($in_both)).Length

$x = @" 
Current STIG: $($current_version_stig_name)
New STIG    : $($new_release_stig_name)

---------------------------
In new NOT in current. ($($only_in_new_count))
---------------------------
$(
$count = 0
foreach($v in $only_in_new)
{
    if($v.Status -ne 'Not A Finding' -and $v.Status -ne 'Not Applicable')
    {
        $count += 1
        $("($($count)) $($v.'Vuln ID')" | Out-String)
    }
}
)
---------------------------

---------------------------
In current NOT in new. ($($only_in_current_count))
---------------------------
$(
$count = 0
foreach($v in $only_in_current)
{
    if($v.Status -ne 'Not A Finding' -and $v.Status -ne 'Not Applicable')
    {
        $count += 1
        $("($($count)) $($v.'Vuln ID')" | Out-String)
    }
}
)
---------------------------

---------------------------
In both current AND new. ($($in_both_count))
---------------------------
$($count = 0
foreach($v in $in_both)
{
    $count += 1
    $("($($count)) $($v.'Vuln ID')" | Out-String)
}
)
---------------------------
"@
$x | Out-File -FilePath $($compare_file)

}

function Get-FileName($initialDirectory)
{   
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") |
    Out-Null

    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "All files (*.*)| *.*"
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filename
}

###############
# Entry Point #
###############

#########################################################################################
# POAM documents section specific information. Change these as needed for each section. #
#########################################################################################
$organization = "SDNG CIO"
$date = (Get-Date -format "yyyy-MMM-dd") # Use this line or comment out this line and use your own date format on a different line. Example: $date = "August 14th, 2017"
$poc = "WO1 Hanisch, Ashton"
$section = "NGSD-IMO-SA"
$responsibility = "SA"
#########################################################################################

$run_date = (Get-Date -UFormat "%Y-%m-%d_%H-%M-%S")
$log_path = ".\output\__logs\$($run_date)\$($run_date)_YASP.log"
$error_path = ".\output\__logs\$($run_date)\$($run_date)_YASP_errors.log"
$script_name = $($MyInvocation.MyCommand.Name)

if($d)
{
    cls
    Write-Host "[^] Document generation parameter specified. Generating documents now." -ForegroundColor Cyan

    # Start logging
    Start-Transcript -Path $($log_path)

    Try
    {
        Generate-Documents -run_date $($run_date) -organization $($organization) -date $($date) -poc $($poc) -section $($section) -responsibility $($responsibility)
        
        if($?)
        {
            Write-Host ""
            Write-Host "[*] Document generation finished successfully! Check out your starter documents at .\output\__poams." -ForegroundColor Green
            Write-Host ""
        }
    }

    Catch
    {
        $_ | Out-File -Append $($error_path)
        Write-Host ""
        Write-Host "[!] Document generation failed. Check out the error logs at .\output\__logs\$($run_date)\$($run_date)_YASP_errors.log." ([char]7)  -ForegroundColor Red
        Write-Host ""
    }

    # Stop logging
    Stop-Transcript
}
elseif($s)
{
    cls
    Write-Host "[^] Statistics generation parameter specified. Calculating statistics now." -ForegroundColor Cyan

    # Start logging
    Start-Transcript -Path $($log_path)

    # Catch errors and write them to time stamp error file
    Try
    {
        Generate-Statistics -run_date $($run_date)
        
        if($?)
        {
            Write-Host ""
            Write-Host "[*] Statistics generation finished successfully! Check out your statistics files at .\output\__statistics\$($run_date)." -ForegroundColor Green
            Write-Host ""
        }
    }

    Catch
    {
        $_ | Out-File -Append $($error_path)
        Write-Host ""
        Write-Host "[!] Statistics generation failed. Check out the error logs at .\output\__logs\$($run_date)\$($run_date)_YASP_errors.log." ([char]7)  -ForegroundColor Red
        Write-Host ""
    }

    # Stop logging
    Stop-Transcript
}
elseif($c)
{
    cls
    Write-Host "[^] Compare versions parameter specified. Comparing versions now." -ForegroundColor Cyan

    # Start logging
    Start-Transcript -Path $($log_path)

    # Catch errors and write them to time stamp error file
    Try
    {
        # Light input validation for current .csv file input. Checking to make sure they put something into the dialog box prompt, giving them 3 tries until exit.
        $attempt_count = 0
        Do { 
            $attempt_count += 1

            Write-Host ""
            Write-Host "[#] Select CURRENT version file. Attempt [$($attempt_count)/3]" -ForegroundColor Yellow
            $current_version = Get-FileName

            if(!($($current_version)) -and $($attempt_count) -eq 3)
            {
                Write-Host ""
                Write-Host "[!] No CURRENT version file selected. Make sure to select your CURRENT version file next time." -ForegroundColor Red
                throw "[!] No CURRENT version file selected. Make sure to select your CURRENT version file next time."
                exit 1
            }
        }
        while($current_version -eq "" -and $attempt_count -ne 3)

        # Light input validation for new .csv file input. Checking to make sure they put something into the dialog box prompt, giving them 3 tries until exit.
        $attempt_count = 0
        Do { 
            $attempt_count += 1

            Write-Host ""
            Write-Host "[#] Select NEW version file. Attempt [$($attempt_count)/3]" -ForegroundColor Yellow
            $new_release = Get-FileName

            if(!($($new_release)) -and $($attempt_count) -eq 3)
            {
                Write-Host ""
                Write-Host "[!] No NEW version file selected. Make sure to select your NEW version file next time." -ForegroundColor Red
                throw "[!] No NEW version file selected. Make sure to select your NEW version file next time."
                exit 1
            }
        }
        while($new_release -eq "" -and $attempt_count -ne 3)

        # Compare versions once input validation passes.
        Compare-Versions -current $current_version -new $new_release -run_date $run_date
        
        if($?)
        {
            Write-Host ""
            Write-Host "[*] Comparing versions finished successfully! Check out your comparison files at .\output\__compare\$($run_date)." -ForegroundColor Green
            Write-Host ""
        }
    }

    Catch
    {
        $_ | Out-File -Append $($error_path)
        Write-Host ""
        Write-Host "[!] Comparing versions failed. Check out the error logs at .\output\__logs\$($run_date)\$($run_date)_YASP_errors.log." ([char]7)  -ForegroundColor Red
        Write-Host ""
    }

    # Stop logging
    Stop-Transcript
}
elseif($h)
{
    cls
    Write-Host ""
    Write-Host "[^] Help parameter specified. Presenting full help now." -ForegroundColor Cyan
    Get-Help .\$($script_name) -Full
}
else
{
    cls
    Write-Host "[!] Run command: '.\$($script_name) -h' or 'Get-Help .\$($script_name) -Full' to get detailed help information." ([char]7) -ForegroundColor Red
}