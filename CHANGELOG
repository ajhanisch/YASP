------------------------------------
CHANGELOG
------------------------------------
version: 1.9
date: 2/14/2018
CHANGES:
- replaced .Replace method calls with -replace due to errors being thrown
------------------------------------
version: 1.8
date: 9/27/2017
CHANGES:
- modified text file creation from multiple append commands to using "here" strings with sub expression
- added detailed csv output file to statistics generation
------------------------------------
version: 1.7
date: 9/15/2017
CHANGES:
- added .html output of statistics
------------------------------------
version: 1.6
date: 9/8/2017
CHANGES:
- added a .csv to statistics output containing cat I, cat II, cat III, compliance score, and compliance status
------------------------------------
version: 1.5
date: 9/7/2017
CHANGES:
- added "elapsed time" functionality to "POAM generation"
- added "elapsed time" display to "poam generation" progress display
- added option to exit or continue POAM generation process
- modified primary array populating functionality into separate function to be called by other functions when needed
------------------------------------
version: 1.4
date:9/6/2016
CHANGES:
- added light input validation to "compare versions" functionality
------------------------------------
version: 1.3
date: 9/1/2017
CHANGES
- added "comparison" functionality parameter to compare current and new release of STIG
- modified source code function order to mirror help menu order of presentation
- added progress bar functionality in soure code as option to display POAM generation status
------------------------------------
version: 1.2
date: 8/31/2017
CHANGES
- added "compliance status" calculation and presentation to total and stats file including color based text output
- added additional event based colors and symbols to script output
- corrected poam generation logic to also exclude a status of "not applicable" (was excluding only "not a finding" prior to this correction)
- corrected statistic generation output to contain only unique values rather than total to reflect more accurate and easily readable results
------------------------------------
version: 1.1
date: 8/30/2017
CHANGES
- added functionality to gather effected hosts of each vuln_id
- added "effected hosts" section to POAM document
- modified generation functionality to only generate unique POAM's based on vuln_id then to include "effected hosts" section to contain all effected hosts of vuln_id
- added "finding details" and "comments" to POAM generation
------------------------------------
version: 1.0
date: 8/25/2017
CHANGES
- corrected how "already created" and "need to create" poams statistics were calculated and presented
- added event based colors to output text
- added an "ETA" calculation and presentation for when POAM creation should finish
------------------------------------
version: 0.9
date: 8/24/2017
CHANGES
- added logic to avoid "divide by zero" error that was happening when calculating and displaying compliance scores
- added logic to calculate and display compliance scores properly no matter how many "not a finding" a .csv has
------------------------------------
version: 0.8
date: 8/10/2017
CHANGES
- modified script descriptions and help functionality to include changelog and wishlist
------------------------------------
version: 0.7
date: 8/9/2017
CHANGES
- added functionality to create poam document only if vuln_id does not already exists (new STIG version / release functionality)
- modified mathematical output to be correct during poam creation when there are already documents and when there are not
- modified source code to be more organized
- added parameter functionality (document generation, statistics generation, help menu)
------------------------------------
version: 0.6
date: 8/8/2017
CHANGES
- corrected counting displays sometimes not appearing when 0 or 1 was returned due to flatening
- added functionality to test if vuln_id exists in each stig folder, it not create it, if yes skip it
------------------------------------
version: 0.5
date: 8/4/2017
CHANGES
- modified output to include working signature blocks (currently requires signature_template.docx in same folder as script to function) not ideal solution, but works for now
------------------------------------
version: 0.4
date: 8/3/2017
CHANGES
- modified output from copied pdf and guts .txt file to populated word documents
------------------------------------
version: 0.3
date: 7/28/2017
CHANGES
- modified individual and overal stats counting / presentation to look better
- added STIG counting functionality to presentation
------------------------------------
version: 0.2
date: 7/27/2017
CHANGES
- added date folder to output so it can be ran without fear of overwriting/other issues
- corrected counting/statistics problems
- added overall counting for all parsed STIG's to output folder in stats_file.txt (includes counts for each STIG and total for all)
------------------------------------
version: 0.1
date: 7/6/2017
CHANGES
- initial functionality created
------------------------------------